from django.contrib import admin
from .models import *


admin.site.register(AboutMe)
admin.site.register(Servis)
admin.site.register(Skills)
admin.site.register(Informations)
admin.site.register(Experience)
admin.site.register(Testimonials)
admin.site.register(RecentPost)
admin.site.register(Category)
admin.site.register(Portfolio)
admin.site.register(Contact)



