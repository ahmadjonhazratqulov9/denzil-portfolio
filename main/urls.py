from django.urls import path
from .views import Index, Message

urlpatterns=[
    path('', Index, name='index'),
    path('message/', Message, name='message'),
]


