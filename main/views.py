from django.shortcuts import render,  redirect
from .models import *



def Index(request):
    servises = Servis.objects.all().order_by('-id')[0:6]
    about = AboutMe.objects.all()
    skill = Skills.objects.all()
    info = Informations.objects.all()
    exp = Experience.objects.all()
    test = Testimonials.objects.all()
    post = RecentPost.objects.all()
    cat = Category.objects.all()
    portfolio = Portfolio.objects.all()


    context = {
        'servises': servises,
        'about': about,
        'skill': skill,
        'info': info,
        'exp': exp,
        'test': test,
        'post': post,
        'cat': cat,
        'portfolio': portfolio,



    }
    return render(request, 'index.html', context)




def Message(request):

    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        subject = request.POST['subject']
        msg = request.POST['message']
        m = Contact.objects.create(
            name=name,
            email=email,
            subject=subject,
            msg=msg,
        )
        m.save()
        return redirect('index.html')


