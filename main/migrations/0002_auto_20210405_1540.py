# Generated by Django 3.1.7 on 2021-04-05 10:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='servis',
            options={'verbose_name_plural': 'Servislar'},
        ),
    ]
