from django.db import models


class AboutMe(models.Model):
    image = models.ImageField(upload_to='media')
    title = models.CharField(max_length=255)
    text = models.TextField()
    name = models.CharField(max_length=255)
    bith_day = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Men haqimda"




class Servis(models.Model):
     icon = models.CharField(max_length=255)
     title = models.CharField(max_length=50)
     text = models.TextField()

     def __str__(self):
         return self.title
     class Meta:
         verbose_name_plural = "Servislar"



class Skills(models.Model):
    text = models.TextField()
    skill_name = models.CharField(max_length=255)
    number = models.IntegerField()

    def __str__(self):
        return self.skill_name

    class Meta:
        verbose_name_plural = "Qobilyatlar"

class Informations(models.Model):
    icon = models.CharField(max_length=255)
    number = models.IntegerField()
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Informatsiya"

class Experience(models.Model):
    data_time = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    text = models.TextField()

    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Tajriba"


class Testimonials(models.Model):
    text = models.TextField()
    image = models.ImageField(upload_to='media/')
    name = models.CharField(max_length=255)
    lavel = models.CharField(max_length=255)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Izoh uchun"



class RecentPost(models.Model):
    notification = models.CharField(max_length=255)
    image = models.ImageField(upload_to='media/')
    title = models.CharField(max_length=255)
    data_time = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Oxirgi postlar"


class Category(models.Model):
    category_name = models.CharField(max_length=255)

    def __str__(self):
        return self.category_name

    class Meta:
        verbose_name_plural = "Kategoriyalar"


class Portfolio(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='media/')
    title = models.CharField(max_length=255)
    text = models.TextField()

    def __str__(self):
        return f"{self.category} | {self.title}"

    class Meta:
        verbose_name_plural = "Porfolio ishlarim"


class Contact(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    subject = models.CharField(max_length=255)
    msg = models.TextField()

    def __str__(self):
        return f"{self.name} | {self.email}"

    class Meta:
        verbose_name_plural = "Xabarlar"






